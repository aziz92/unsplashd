//
//  FavoritesRepo.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

class FavoritesRepo: ObservableObject {
    @Published var photos = [Photo]()
}
