//
//  UnsplashAPI.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

class UnsplashAPI: ObservableObject {
    
    func getPopular() async -> [Photo] {
        guard let url = URL(string: "https://api.unsplash.com/photos?client_id=hwXckQvWu6aIN483v5tf2NbrXe1mn-xOSmjV4niKYPY") else {
            print("Invalid URL")
            return []
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            
            if let decodedResponse = try? JSONDecoder().decode([Photo].self, from: data) {
                return decodedResponse
            }
            
            return []
        } catch {
            print("Invalid data")
            return []
        }
        
    }
    
    func search(_ query: String) async -> [Photo] {
        guard let url = URL(string: "https://api.unsplash.com/search/photos?query=\(query)&client_id=hwXckQvWu6aIN483v5tf2NbrXe1mn-xOSmjV4niKYPY") else {
            print("Invalid URL")
            return []
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            
            if let decodedResponse = try? JSONDecoder().decode(Response.self, from: data) {
                return decodedResponse.results
            }
            
            return []
        } catch {
            print("Invalid data")
            return []
        }
        
    }
}
