//
//  MockAPI.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

class MockAPI: UnsplashAPI {
    
    private let photos: [Photo]
    
    init(photos: [Photo] = [.kite, .kite2, .kite3, .kite4]) {
        self.photos = photos
    }
    
    override func getPopular() async -> [Photo] {
        return photos
    }
    
    override func search(_ query: String) async -> [Photo] {
        return photos
    }
}
