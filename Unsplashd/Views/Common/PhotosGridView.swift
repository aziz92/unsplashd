//
//  PhotosGridView.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import SwiftUI

struct PhotosGridView: View {
    @Binding var photos: [Photo]
    
    private let columns = [
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
        GridItem(.flexible(), spacing: 1),
    ]
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns, spacing: 1) {
                ForEach(photos, id: \.self) { photo in
                    NavigationLink(value: photo) {
                        PhotoCell(photo: photo)
                    }
                }
            }
        }
    }
}

struct PhotosGridView_Previews: PreviewProvider {
    static var previews: some View {
        PhotosGridView(photos: .constant(Photo.mocks))
    }
}
