//
//  PhotoView.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import SwiftUI

struct PhotoView: View {
    @State var photo: Photo
    @EnvironmentObject var favoritesRepo: FavoritesRepo
    
    var body: some View {
        Color.clear.overlay(
            AsyncImage(url: URL(string: photo.urls.thumb)) { phase in
                switch phase {
                case .success(let image):
                    image
                        .resizable()
                        .scaledToFit()
                default:
                    Color(UIColor.init(hexaString: photo.color))
                        .opacity(0.3)
                }
            }
        )
        .ignoresSafeArea()
    }
}

struct PhotoView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoView(photo: .kite)
            .environmentObject(FavoritesRepo())
    }
}
