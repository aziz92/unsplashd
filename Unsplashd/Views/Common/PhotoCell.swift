//
//  PhotoCell.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import SwiftUI

struct PhotoCell: View {
    @State var photo: Photo
    
    var body: some View {
        Color.clear.overlay(
            AsyncImage(url: URL(string: photo.urls.thumb)) { phase in
                switch phase {
                case .success(let image):
                    image
                        .resizable()
                        .scaledToFill()    // << for image !!
                default:
                    Color(UIColor.init(hexaString: photo.color))
                        .opacity(0.3)
                }
            }
        )
        .frame(maxWidth: .infinity)
        .aspectRatio(1, contentMode: .fit) // << for square !!
        .clipped()
    }
}

struct PhotoCell_Previews: PreviewProvider {
    static var previews: some View {
        PhotoCell(photo: .kite).previewLayout(.sizeThatFits)
    }
}
