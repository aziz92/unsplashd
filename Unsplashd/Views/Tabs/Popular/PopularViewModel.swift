//
//  PopularViewModel.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

@MainActor class PopularViewModel: ObservableObject {
    
    @Published var photos = [Photo]()
    var api: UnsplashAPI?
    
    func fetch() async {
        guard let api = api else {return}
        await photos = api.getPopular()
    }
}
