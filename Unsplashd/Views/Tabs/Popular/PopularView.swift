//
//  PopularView.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import SwiftUI

struct PopularView: View {
    
    @StateObject private var viewModel = PopularViewModel()
    @EnvironmentObject var api: UnsplashAPI
        
    var body: some View {
        NavigationStack {
            PhotosGridView(photos: $viewModel.photos)
                .navigationTitle("Popular")
                .navigationDestination(for: Photo.self) { photo in
                    PhotoView(photo: photo)
                }
        }
        .onAppear {
            viewModel.api = api
        }
        .task {
            await viewModel.fetch()
        }
    }
}

struct PopularView_Previews: PreviewProvider {
    static var previews: some View {
        PopularView()
            .environmentObject(UnsplashAPI())
    }
}
