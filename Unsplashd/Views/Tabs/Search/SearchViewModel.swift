//
//  SearchViewModel.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

@MainActor class SearchViewModel: ObservableObject {
    
    @Published var query = "" {
        didSet {
            Task.init {await search()}
        }
    }
    @Published var photos = [Photo]()
    
    var api: UnsplashAPI?
    
    private func search() async {
        guard let api = api else {return}
        await photos = api.search(query)
        
    }
}
