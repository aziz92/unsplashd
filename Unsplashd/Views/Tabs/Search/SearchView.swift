//
//  SearchView.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import SwiftUI

struct SearchView: View {
    @StateObject private var viewModel = SearchViewModel()
    @EnvironmentObject var api: UnsplashAPI
    
    var body: some View {
        NavigationStack {
            PhotosGridView(photos: $viewModel.photos)
                .searchable(text: $viewModel.query)
                .navigationDestination(for: Photo.self) { photo in
                    PhotoView(photo: photo)
                }
        }
//        .onChange(of: $viewModel.query) { newQuery in
//            Task.init { await $viewModel.search(query) }
//        }
        .onAppear {
            viewModel.api = api
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
