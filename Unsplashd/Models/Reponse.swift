//
//  Reponse.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

struct Response: Codable {
    let results: [Photo]
}
