//
//  Photo.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

struct Photo: Codable, Hashable {
    let id: String
    let color: String
    
    let urls: PhotoURL
}

extension Photo {
    static let kite: Self = .init(id: "BGqfOqMVCyk", color: "#f3a659", urls: PhotoURL(thumb: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1627068477565-3a66d5f76d5e", full: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1627068477565-3a66d5f76d5e"))
    
    static let kite2: Self = .init(id: "WE7qvb0nqEA", color: "#c0d9c0", urls: PhotoURL(thumb: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1564499504739-bc4fc2ae8cba", full: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1564499504739-bc4fc2ae8cba"))
    
    static let kite3: Self = .init(id: "rFXDDiR4ub4", color: "#73a6c0", urls: PhotoURL(thumb: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1627988114935-406dadb71002", full: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1627988114935-406dadb71002"))
    
    static let kite4: Self = .init(id: "ZWSWyNLZ13g", color: "#73a6c0", urls: PhotoURL(thumb: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1619092888643-241ab257a167", full: "https://s3.us-west-2.amazonaws.com/images.unsplash.com/small/photo-1619092888643-241ab257a167"))
    
    //ZWSWyNLZ13g
}

extension Photo {
    static let mocks: [Self] = [.kite, .kite2, .kite3, .kite4]
}
