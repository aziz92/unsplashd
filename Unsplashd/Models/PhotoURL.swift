//
//  PhotoURL.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import Foundation

struct PhotoURL: Codable, Hashable {
    let thumb: String
    let full: String
}
