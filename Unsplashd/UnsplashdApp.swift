//
//  UnsplashdApp.swift
//  Unsplashd
//
//  Created by Aziz UdDin on 2022-06-24.
//

import SwiftUI

@main
struct UnsplashdApp: App {
    let unsplashAPI: UnsplashAPI = MockAPI()
    let favoritesRepo = FavoritesRepo()
    
    var body: some Scene {
        
        WindowGroup {
            TabView {
                PopularView()
                    .tabItem {
                        Label("Popular", systemImage: "flame")
                    }
                
                SearchView()
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass")
                    }
//                FavoritesView()
//                    .tabItem {
//                        Label("Favorites", systemImage: "star")
//                    }
            }
            .environmentObject(unsplashAPI)
            .environmentObject(favoritesRepo)
        }
    }
}
