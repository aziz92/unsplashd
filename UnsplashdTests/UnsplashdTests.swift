//
//  UnsplashdTests.swift
//  UnsplashdTests
//
//  Created by Aziz UdDin on 2022-06-24.
//

import XCTest
@testable import Unsplashd

@MainActor class UnsplashdTests: XCTestCase {
    
    private var api: UnsplashAPI = MockAPI(photos: [.kite, .kite2])
    private var viewModel = PopularViewModel()

    override func setUpWithError() throws {
        viewModel.api = api
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPopularScreen() async throws {
        XCTAssertTrue(viewModel.photos.count == 0)
        await viewModel.fetch()
        XCTAssertTrue(viewModel.photos.count == 2)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
