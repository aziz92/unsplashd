//
//  SearchViewModelTests.swift
//  UnsplashdTests
//
//  Created by Aziz UdDin on 2022-06-24.
//

import XCTest
@testable import Unsplashd

@MainActor final class SearchViewModelTests: XCTestCase {
    
    private var api: UnsplashAPI = MockAPI(photos: [.kite, .kite2, .kite3, .kite4])
    private var viewModel = SearchViewModel()
    
    override func setUpWithError() throws {
        viewModel.api = api
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    @MainActor func testSearchFeature() async throws {
        XCTAssertTrue(viewModel.photos.count == 0)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
